package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTInnerTextCss;
import cc.pptshow.ppt.element.impl.PPTInnerLine;
import cc.pptshow.ppt.element.impl.PPTInnerText;
import cc.pptshow.ppt.element.impl.PPTText;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 在一个PPT页面中创建一个文本
 */
public class BuildText {

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();

        //创建一个行内文本对象，文字设定为Hello World
        PPTInnerText pptInnerText = PPTInnerText.build("Hello World");
        //创建一个行内文本样式对象，让文本颜色为红色
        PPTInnerTextCss pptInnerTextCss = PPTInnerTextCss.build()
                .setColor("FF00000")
                .setFontSize(72)
                .setBold(true) //加粗
                .setItalic(true) //倾斜
                .setFontFamily("微软雅黑");
        //绑定行内文本和样式对象
        pptInnerText.setCss(pptInnerTextCss);

        //通过行内文本创建一个行文本对象，并通过行文本对象创建文本对象
        PPTText pptText = PPTText.build(PPTInnerLine.build(pptInnerText));
        //在PPT页面中添加文本对象
        side.add(pptText);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildText.pptx");
    }

}
