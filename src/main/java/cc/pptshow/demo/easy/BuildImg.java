package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTImgCss;
import cc.pptshow.ppt.element.impl.PPTImg;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 在一个PPT页面增加一个图片
 */
public class BuildImg {

    /**
     * 这里自己准备张图片，把这个地址改掉
     */
    private static final String IMG_PATH = DESKTOP_PATH + "demo.png";

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();

        //新建一个PPT图片对象
        PPTImg pptImg = PPTImg.build(IMG_PATH);
        //配置样式
        PPTImgCss pptImgCss = PPTImgCss.build()
                .setAngle(45)
                .setLeft(2)
                .setTop(3)
                .setWidth(5)
                .setHeight(6);
        //绑定样式和图片对象
        pptImg.setCss(pptImgCss);

        //在PPT页面中添加图片对象
        side.add(pptImg);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildImg.pptx");
    }

}
