package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTShapeCss;
import cc.pptshow.ppt.domain.background.ColorBackGround;
import cc.pptshow.ppt.domain.border.ColorBorder;
import cc.pptshow.ppt.domain.shape.RoundRect;
import cc.pptshow.ppt.element.impl.PPTShape;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 在PPT中添加一个图形元素
 */
public class BuildShape {

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();

        //创建一个图形对象
        PPTShape pptShape = new PPTShape();
        //创建一个样式表
        PPTShapeCss pptShapeCss = new PPTShapeCss()
                //设置形状，比如这里设置是圆角矩形，圆角程度是短边长度的20%
                .setShape(new RoundRect().setFillet(20))
                .setWidth(10)
                .setHeight(5)
                .setLeft(8)
                .setTop(8)
                .setAngle(45)
                .setBackground(ColorBackGround.buildByColor("FF0000"))
                .setBorder(new ColorBorder().setWidth(2).setColor("333333"));
        //绑定样式表和图形对象
        pptShape.setCss(pptShapeCss);

        //在PPT页面中添加图形对象
        side.add(pptShape);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildShape.pptx");
    }

}
