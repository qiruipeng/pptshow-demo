package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTLineCss;
import cc.pptshow.ppt.element.impl.PPTLine;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 在一个PPT页面增加一个线段
 */
public class BuildLine {

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();

        //创建一个线段
        PPTLine pptLine = new PPTLine();
        //创建样式表对象
        PPTLineCss pptLineCss = new PPTLineCss()
                .setTop(3)
                .setLeft(2)
                .setWidth(10)
                .setHeight(9)
                .setLineWidth(5)
                .setColor("FF0000")
                //默认线条从左上到右下，如果需要右上到左下，在这里如下设置
                .setType(PPTLineCss.LineType.TOP_RIGHT_BOTTOM_LEFT);
        //绑定样式表和线段对象
        pptLine.setCss(pptLineCss);

        //在PPT页面中添加线段对象
        side.add(pptLine);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildLine.pptx");
    }

}
