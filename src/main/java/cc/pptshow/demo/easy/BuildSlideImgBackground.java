package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTInnerTextCss;
import cc.pptshow.ppt.domain.PPTSideCss;
import cc.pptshow.ppt.domain.background.ImgBackground;
import cc.pptshow.ppt.element.impl.PPTInnerLine;
import cc.pptshow.ppt.element.impl.PPTInnerText;
import cc.pptshow.ppt.element.impl.PPTText;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 设置PPT页面是图片背景
 */
public class BuildSlideImgBackground {

    /**
     * 这里自己准备张图片，把这个地址改掉
     */
    private static final String IMG_PATH = DESKTOP_PATH + "bg-demo.jpg";

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();

        //*****【先创建个文本对象，每一页都加上这个文本元素模拟正常页面内容】******

        //创建一个行内文本对象，文字设定为Hello World
        PPTInnerText pptInnerText = PPTInnerText.build("Hello World");
        //创建一个行内文本样式对象，让文本颜色为红色
        PPTInnerTextCss pptInnerTextCss = PPTInnerTextCss.build()
                .setColor("FF00000")
                .setFontSize(72)
                .setBold(true) //加粗
                .setItalic(true) //倾斜
                .setFontFamily("微软雅黑");
        //绑定行内文本和样式对象
        pptInnerText.setCss(pptInnerTextCss);
        //通过行内文本创建一个行文本对象，并通过行文本对象创建文本对象
        PPTText pptText = PPTText.build(PPTInnerLine.build(pptInnerText));

        //*****【以下为核心代码】******

        //*****【纯色背景】******
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();
        //复制新的一页，把刚才的文本放到页面里面，多个页面使用同一个对象记得clone哦，避免修改一个其他跟着修改
        //pptShow内的clone均为深克隆
        side.add(pptText.clone());
        //创建一个页面样式表
        PPTSideCss pptSideCss = new PPTSideCss().setBackground(new ImgBackground().setImg(IMG_PATH));
        //绑定页面和样式表
        side.setCss(pptSideCss);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildSlideImgBackground.pptx");
    }

}
