package cc.pptshow.demo.easy;

import cc.pptshow.ppt.domain.PPTShapeCss;
import cc.pptshow.ppt.domain.background.ColorBackGround;
import cc.pptshow.ppt.domain.border.ColorBorder;
import cc.pptshow.ppt.domain.shape.RoundRect;
import cc.pptshow.ppt.domain.shape.SelfShape;
import cc.pptshow.ppt.element.impl.PPTShape;
import cc.pptshow.ppt.show.PPTShow;
import cc.pptshow.ppt.show.PPTShowSide;

import static cc.pptshow.demo.contant.Constant.DESKTOP_PATH;

/**
 * 在PPT中添加一个自定义的图形元素
 * 这是不支持的图形临时解决方案，后续可能更改或取消换其他实现方法
 * 建议不要过度依赖此功能
 *
 * 传入XML提取方法：画出来图形，把PPTX文件后缀改zip，解压，到ppt/slides目录下找到你画图形的那页PPT取出<a:custGeom>标签内容即可
 */
public class BuildSelfShape {

    public static void main(String[] args) {
        //新建一个PPT对象
        PPTShow pptShow = PPTShow.build();
        //新建一页PPT
        PPTShowSide side = PPTShowSide.build();

        //创建一个图形对象
        PPTShape pptShape = new PPTShape();
        //创建一个样式表
        PPTShapeCss pptShapeCss = new PPTShapeCss()
                //设置形状，这里是从做好的PPT里面复制出来的
                .setShape(new SelfShape().setCustGeom("<a:custGeom ><a:avLst /><a:gdLst /><a:ahLst /><a:cxnLst>" +
                        "<a:cxn ang=\"3\"><a:pos x=\"hc\" y=\"t\" /></a:cxn><a:cxn ang=\"cd2\"><a:pos x=\"l\" " +
                        "y=\"vc\" /></a:cxn><a:cxn ang=\"cd4\"><a:pos x=\"hc\" y=\"b\" /></a:cxn><a:cxn ang=\"0\">" +
                        "<a:pos x=\"r\" y=\"vc\" /></a:cxn></a:cxnLst><a:rect l=\"l\" t=\"t\" r=\"r\" b=\"b\" />" +
                        "<a:pathLst><a:path w=\"3999\" h=\"4068\"><a:moveTo><a:pt x=\"0\" y=\"0\" /></a:moveTo><a:lnTo>" +
                        "<a:pt x=\"2865\" y=\"0\" /></a:lnTo><a:lnTo><a:pt x=\"3575\" y=\"0\" /></a:lnTo><a:lnTo>" +
                        "<a:pt x=\"3575\" y=\"442\" /></a:lnTo><a:lnTo><a:pt x=\"3999\" y=\"442\" /></a:lnTo><a:lnTo>" +
                        "<a:pt x=\"3999\" y=\"3410\" /></a:lnTo><a:lnTo><a:pt x=\"3999\" y=\"4068\" /></a:lnTo><a:lnTo>" +
                        "<a:pt x=\"0\" y=\"4068\" /></a:lnTo><a:lnTo><a:pt x=\"0\" y=\"0\" /></a:lnTo><a:close />" +
                        "</a:path></a:pathLst></a:custGeom>"))
                .setWidth(7.05)
                .setHeight(7.18)
                .setLeft(8)
                .setTop(8)
                .setBackground(ColorBackGround.buildByColor("FF0000"));
        //绑定样式表和图形对象
        pptShape.setCss(pptShapeCss);

        //在PPT页面中添加图形对象
        side.add(pptShape);
        //在PPT里面添加PPT页面
        pptShow.add(side);

        //输出到文件
        pptShow.toFile(DESKTOP_PATH + "BuildSelfShape.pptx");
    }

}
