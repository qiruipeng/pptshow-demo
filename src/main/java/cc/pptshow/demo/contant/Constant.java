package cc.pptshow.demo.contant;

import javax.swing.filechooser.FileSystemView;


/**
 * 重要：本Demo建议Windows使用，会自动输出到桌面！
 * 其他系统用户清先修改输出文件路径~ 避免生成后找不到
 */
public class Constant {

    /**
     * 输出到哪个文件夹
     */
    public static final String DESKTOP_PATH = FileSystemView.getFileSystemView()
            .getHomeDirectory().getAbsolutePath() + "\\";

}
